var express = require('express');
var app = express();
var http = require('http').Server(app);
var firebase = require('firebase');
var fs = require('fs');
var obj = [];
var port = process.env.PORT || 31;
var CronJob = require('cron').CronJob;


const produccion = {
    apiKey: "AIzaSyC2dxeazyHNSTAuimIhFaVXt8AYZV3aub4",
    authDomain: "sorteoschama.firebaseapp.com",
    databaseURL: "https://sorteoschama.firebaseio.com",
    projectId: "sorteoschama",
    storageBucket: "sorteoschama.appspot.com",
    messagingSenderId: "214342016835",
    appId: "1:214342016835:web:68bede8357c4936f29d9f6"
};

const develop = {
    apiKey: "AIzaSyBEf9-h8CuW9cNjiJo8uLnMfbcBjgPxG9Y",
    authDomain: "developchamaclub.firebaseapp.com",
    databaseURL: "https://developchamaclub.firebaseio.com",
    projectId: "developchamaclub",
    storageBucket: "developchamaclub.appspot.com",
    messagingSenderId: "326557585019",
    appId: "1:326557585019:web:30b4aea596eb27fa74dcde"
};

//configuracion servidor
app.use(express.static('./'));
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});
//configuracion inicializacion botones
//iniciar servidor http
http.listen(port, function () {
    console.log('Se inició el server! Puerto :' + port);
    console.log(obtenerHora());
});
tiempoGanador = 20000
tiempoSorteando = 30000
fb = firebase.initializeApp(produccion);
db = fb.firestore();
idJornada = ""
idGanador = ""
nombreGanador = ""
rutGanador = ""
idGanador2 = ""
nombreGanador2 = ""
rutGanador2 = ""
contador = 0
local = 'osorno'
datos = {}
premioMinutoFeliz = ""
rutMinutoFeliz = ""
function leerClientes() {
    console.log("leyendo datos")
    fs.readFile('clientes4.json', 'utf8', function readFileCallback(err, data) {
        if (err) {
            console.log(err);
        } else {
            datos = JSON.parse(data); //now it an object
        }

        datos.forEach((dat) => {
            db.collection("clientes/"+dat.sucursal).doc(dat.id).set({
                clasificacion: dat.clasificacion,
                nombre: dat.nombre,
                nombre: dat.nombre,
                apellido: dat.apellido,
                contador: dat.contador,
                cupon: dat.cupon,
                deshabilitado: false,
                estado: dat.estado,
                fecha: dat.fecha,
                hora: dat.hora,
                monto: dat.monto,
                rut: dat.rut,
                sucursal: dat.sucursal,
                ganador: false
            })
        });

    });

}


function exportarClientes() {
    console.log("exportandoclientes")
    db.collection("clientes/").onSnapshot(function (snap) {
        snap.forEach((queryCup) => {
            obj.push({
                id: queryCup.id,
                nombre: queryCup.data().nombre,
                clasificacion: queryCup.data().clasificacion,
                apellido: queryCup.data().apellido,
                contador: queryCup.data().contador,
                cupon: queryCup.data().cupon,
                deshabilitado: false,
                estado: queryCup.data().estado,
                fecha: queryCup.data().fecha,
                hora: queryCup.data().hora,
                monto: queryCup.data().monto,
                rut: queryCup.data().rut,
                sucursal: queryCup.data().sucursal,
                ganador: false
            })
        })
        var json = JSON.stringify(obj);
        fs.writeFile('clientes4.json', json, 'utf8', () => {
            console.log("listo")
        });


    })

}


// ----------------- funciones Generales
db.collection("cuponeras/").doc(local).onSnapshot(function (queryCup) {
    idJornada = queryCup.data().jornada,
        idGanador2 = queryCup.data().id
    nombreGanador2 = queryCup.data().nombre
    rutGanador2 = queryCup.data().rut
    pantallaCuponera = queryCup.data().cuponera
    teclado = queryCup.data().teclado
    console.log("pantalla Cuponera:" + pantallaCuponera)
    if (contador == 0) {
        sincronizarSorteos(queryCup.data().jornada);
    }
})
limpiarSorteos = () => {
    console.log("ReprogramandoSorteos")
    for (let index = 0; index < listadoSorteos.length; index++) {
        listadoSorteos[index].stop();

    }
}
function sincronizarSorteos(aidi) {
    contador = 1;
    listadoSorteos = [];
    db.collection("sorteos/"+local+"/sorteos").onSnapshot((queryCup) => {
        limpiarSorteos();
        listadoSorteos = [];
        queryCup.forEach((dat) => {
            dia = parseInt(dat.data().fecha.substring(8, 10))
            mes = parseInt(dat.data().fecha.substring(5, 7))
            mes = mes - 1;
            if (dat.data().estado == true) {

                if (dat.data().tipoSorteo == "minutoFeliz") {
                    console.log(dat.data().tipoSorteo + "" + dat.data().horaInicio)
                    cadena = '1 ' + getMinutos(dat.data().horaInicio) + ' ' + getHora(dat.data().horaInicio) + ' * * *'
                    cadena2 = '1 ' + getMinutos(dat.data().horaFinal) + ' ' + getHora(dat.data().horaFinal) + ' * * *'
                    listadoSorteos.push(new CronJob(cadena2,
                        () => {
                            restablecerMin(dat.id, dat.data());
                        },
                        () => {

                        },
                        true
                    ))
                } else {
                    cadena = '1 ' + getMinutos(dat.data().hora) + ' ' + getHora(dat.data().hora) + ' * * *'
                }

                listadoSorteos.push(new CronJob(cadena,
                    () => {
                        if (dat.data().tipoSorteo == "clientes") {
                            console.log("Sorteo tipo Clientes")
                            iniciarSorteoCli(dat.id, dat.data(), dat.data().modalidadSorteo);
                        }
                        if (dat.data().tipoSorteo == "maquinas") {
                            console.log("sorteo tipo maquinas")
                            iniciarSorteoMaq(dat.id, dat.data(), dat.data().modalidadSorteo);
                        }
                        if (dat.data().tipoSorteo == "minutoFeliz") {
                            console.log("sorteo tipo minutoFeliz")
                            iniciarSorteoMin(dat.id, dat.data());
                        }
                    },
                    () => {

                    },
                    true
                ))
            }
        })
    })
}


cambiarSorteando = () => {
    var refLocal = db.collection("cuponeras/"+local);
    return refLocal.update({
        cuponera: "sorteando"
    }).then(() => {
        setTimeout(() => {
            var refLocal = db.collection("cuponeras/"+local)
            return refLocal.update({
                cuponera: "validando"
            })
        }, tiempoSorteando)
    })
}
// ----------------- funciones Generales






// ----------------- Sorteos clientes

iniciarSorteoCli = (id, datos, modalidad) => {
    cupones = []
    //anotamos el premio que se sorteará
    // se cambia la pantalla a sorteando
    if (teclado) {
        cuponera = "preparando"
    } else {

    }
    var refLocal = db.collection("cuponeras/"+local)
    return refLocal.update({
        pantalla: 'sorteando',
        premio: datos.premio,
        cuponera: 'sorteando',
    }).then(() => {

        if (modalidad == "automatico") {
            // no hay ganadorSeleccionado
            lanzarCliSinSelect(id, datos, modalidad)
        } else {
            //si hay ganador seleccionado
            lanzarCliSelect(id, datos, modalidad)
        }
    })
}
lanzarCliSinSelect = (id, datos, modalidad) => {
    console.log("iniciandoSorteoss")
    db.collection("clientes/"+local+"/datos")
        .then((queryCup) => {
            queryCup.forEach((docCup) => {

                if (docCup.data().minuto == true) {
                    console.log("esta participando sin cupon")
                }
                if (docCup.data().deshabilitado == false && docCup.data().estado == true && docCup.data().ganador == false) {
                    cupones.push({
                        id: docCup.id,
                        nombre: docCup.data().nombre,
                        apellido: docCup.data().apellido,
                        rut: docCup.data().rut
                    })

                }
            })
            if (cupones.length > 0) {
                if(cupones.length>5){
                    win = Math.floor(Math.random() * cupones.length);
                }else{
                    win = Math.floor(Math.random() * 4);
                }
                
                rutGanador = cupones[win].rut;
                nombreGanador = cupones[win].nombre + " " + cupones[win].apellido
                idGanador = cupones[win].id
                idCupon = cupones[win].id
                var refLocal = db.collection("cuponeras/"+local)
                return refLocal.update({
                    nombre: nombreGanador,
                    rut: rutGanador,
                    premio: datos.premio,
                    idSorteoActual: id
                }).then(() => {
                    setTimeout(() => {
                        var refDos = db.collection("cuponeras/"+local)
                        return refDos.update({
                            cuponera: "validando",
                            pantalla: "ganador"
                        }).then((datsss) => {
                            setTimeout(() => {
                                var refCli = db.collection("clientes/"+local+"/datos").doc(cupones[win].id);
                                return refCli.update({
                                    estado: false,
                                    ganador: true
                                }).then(() => {
                                    var refSorteo = db.collection("sorteos/"+local+"/sorteos").doc(id);
                                    return refSorteo.update({
                                        estado: false,
                                        rut: rutGanador,
                                        nombre: nombreGanador,
                                        idCliente: cupones[win].id
                                    }).then(() => {
                                        console.log("reiniciando cuponera")
                                        setTimeout(() => {
                                            var refSorteo = db.collection("sorteos/"+local+"/sorteos").doc(id).get().then((data) => {
                                                //pregunta si se lanzo el sorteo anteriormente
                                                console.log("consultando si el sorteo se lanzo antes")
                                                if (data.data().lanzado == true) {
                                                    console.log("no lo hizo")
                                                    var refDos = db.collection("cuponeras").doc(local);
                                                    return refDos.update({
                                                        cuponera: "activa",
                                                        premio: '',
                                                        pantalla: 'listado',
                                                        nombre: "",
                                                        rut: "",
                                                        id: "",
                                                        idSorteoActual: ""
                                                    })
                                                } else {
                                                    var refDos = db.collection("sorteos/"+local+"/sorteos").doc(id);
                                                    return refDos.update({
                                                        lanzado: true

                                                    }).then(() => {
                                                        console.log("si lo hizo, lanzando sorteo denuevo ")
                                                        iniciarSorteoCli(id, datos, modalidad)
                                                    })

                                                }
                                            });


                                        }, tiempoGanador);

                                    })
                                })
                            }, 60000);

                        })


                    }, tiempoSorteando);
                })
            } else {
console.log("lanzando sorteo sin cupones diponibles---------");
resetCliMinutoFeliz();
var refLocal = db.collection("cuponeras/"+local)
return refLocal.update({
    cuponera: 'iniciando',
    pantalla: 'sorteando',
    valorMinuto: "2000",
    premio: "2000",
}).then(() => {
    setTimeout(() => {

        var refLocal = db.collection("cuponeras/"+local)
        return refLocal.update({
            cuponera: 'minutoFeliz',
            nombre: "Minuto Feliz!",
            pantalla: 'ganador',
            valorMinuto: "2000",
            premio: "2000",
        }).then(() => {
            setTimeout(() => {
                var refLocal = db.collection("cuponeras/"+local)
                return refLocal.update({
                    cuponera: 'activa',
                    pantalla: 'listado',
                    valorMinuto: "0",
                    premio: "0",
                }).then(() => {
                    var refDos = db.collection("sorteos/"+local+"/sorteos").doc(id);
                    return refDos.update({
                        nombre: 'minutoFeliz',
                        estado: false,
                        cobrado: true

                    })
                })
            },900000)
        })
    },45000)
})
            }




        })
}
lanzarCliSelect = (id, datos) => {
    var refLocal = db.collection("cuponeras/"+local)
    return refLocal.update({
        nombre: datos.nombre,
        rut: datos.rut,
        premio: datos.premio,
        idSorteoActual: id
    }).then(() => {
        setTimeout(() => {
            var refLocal = db.collection("cuponeras/").doc(local)
            return refLocal.update({
                cuponera: "validando",
                pantalla: "ganador"
            }).then(() => {

                setTimeout(() => {
                    var refCli = db.collection("clientes/"+local+"/datos").doc(datos.id);
                    return refCli.update({
                        estado: false,
                        ganador: true
                    }).then(() => {
                        var refSorteo = db.collection("sorteos/"+local+"/sorteos").doc(id);
                        return refSorteo.update({
                            estado: false
                        }).then(() => {
                            setTimeout(() => {
                                var refDos = db.collection("cuponeras/"+local)
                                return refDos.update({
                                    id: '',
                                    nombre: '',
                                    rut: '',
                                    pantalla: 'listado',
                                    cuponera: "activa"
                                })
                            }, 60000);
                        })

                    }, 60000)
                })

            })
        }, tiempoSorteando);
    })
}

// ----------------- Sorteos clientes
// ----------------- Sorteos Minutos
iniciarSorteoMin = (id, datos, modalidad) => {
    cupones = []
    //anotamos el premio que se sorteará
    // se cambia la pantalla a sorteando
    var refLocal = db.collection("cuponeras/"+local)
    return refLocal.update({
        pantalla: 'sorteando',
        cuponera: 'iniciando',
    }).then(() => {
        resetCliMinutoFeliz();
        sorteoMinuto(id, datos)

    })
}


sorteoMinuto = (id, datos) => {
    var refLocal = db.collection("cuponeras/"+local)
    return refLocal.update({
        nombre: "Minuto Feliz!",
        valorMinuto: datos.premio,
        rut: '',
        premio: datos.premio,
        idSorteoActual: id
    }).then(() => {

        setTimeout(() => {
            var refDos = db.collection("cuponeras/"+local)
            return refDos.update({
                cuponera: "minutoFeliz",
                pantalla: "ganador"
            }).then((datsss) => {
                setTimeout(() => {
                    setTimeout(() => {
                        var refDos = db.collection("cuponeras/"+local)
                        return refDos.update({
                            premio: '',
                            nombre: "  <span style='font-size:130%;'>  Minuto Feliz! </span>     ",
                            rut: "",
                            id: "",
                            idSorteoActual: ""

                        })
                    }, tiempoGanador);
                }, 60000);

            })

        }, 30000);






    })
}
restablecerMin = (idSorteo, datos) => {
    var refLocal = db.collection("cuponeras/"+local)
    return refLocal.update({
        nombre: "",
        rut: '',
        premio: "",
        idSorteoActual: "",
        pantalla: "listado",
        cuponera: "activa",
    }).then(() => {
        console.log("minutoFeliz Finalizado")
        var refDos = db.collection("sorteos/"+local+"/sorteos").doc(idSorteo);
        return refDos.update({
            nombre: "Minuto Feliz!",
            premio: premioMinutoFeliz,
            rut: rutMinutoFeliz
        })
    })
}
resetCliMinutoFeliz = () => {
    db.collection("clientes/"+local+"/datos").get().then((data) => {
        data.forEach((datos) => {
            this.llave = datos.id;
            db.collection("clientes/"+local+"/datos").doc(this.llave).update({
                minuto: false,
                estado: false
            })

        })
    })

}
// -----------------  Sorteos Minutos

// ----------------- Funciones de hora y minutos
getHora = (hora) => {
    return hora.substring(0, 2);
}

getMinutos = (hora) => {
    return hora.substring(3, 5);

}
obtenerHora = () => {
    var ho = new Date();
    hora1 = parseInt(ho.getHours())
    minutos1 = parseInt(ho.getMinutes())
    if (hora1 < 10) {
        hora1 = '0' + hora1
    }
    if (minutos1 < 10) {
        minutos1 = '0' + minutos1
    }
    if (hora1 == '23' && minutos1 == '59') {
        localStorage.controldia = 0;
    }
    horaact = hora1 + ":" + minutos1;
    return horaact;
}
