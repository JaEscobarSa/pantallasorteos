

var config = {
    apiKey: "AIzaSyCzf8hUH6nh0JC8pTz3AYmZN1ZGCLQ3Zq0",
    authDomain: "chamaangular.firebaseapp.com",
    databaseURL: "https://chamaangular.firebaseio.com",
    projectId: "chamaangular",
    storageBucket: "chamaangular.appspot.com",
    messagingSenderId: "396372807247",
    appId: "1:396372807247:web:dd2d8de68b5adb8f"
};
firebase = firebase.initializeApp(config);
db = firebase.firestore();
listClientes = [];
listCupones = [];
db.collection("categorias")
    .onSnapshot(function (querys) {
        querys.forEach(function (doc) {
            console.log(doc.data().nombre)
            clasi = String(doc.data().nombre)
            switch (clasi.toLowerCase()) {
                case 'bronce':
                    localStorage.bronce = doc.data().premio;
                    localStorage.bronceMonto = doc.data().monto;
                    localStorage.contadorBronce = doc.data().contador;
                    localStorage.bronceid = doc.id;
                    break;

                case 'silver':
                    localStorage.silver = doc.data().premio;
                    localStorage.silverMonto = doc.data().monto;
                    localStorage.silverid = doc.id;
                    localStorage.contadorSilver = doc.data().contador;
                    break;

                case 'golden':
                    localStorage.golden = doc.data().premio;
                    localStorage.goldenMonto = doc.data().monto;
                    localStorage.contadorGolden = doc.data().contador;
                    localStorage.goldenid = doc.id;
                    break;

                case 'platinium':
                    localStorage.platinium = doc.data().premio;
                    localStorage.platiniumMonto = doc.data().monto;
                    localStorage.platiniumid = doc.id;
                    localStorage.contadorPlatinium = doc.data().contador;
                    break;

                default:
                    break;
            }


        });
    });


db.collection("clientes").onSnapshot(function (querySnapshot) {
    listClientes = [];
    querySnapshot.forEach(function (doc) {
        listClientes.push({
            id: doc.id,
            nombre: doc.data().nombre,
            apellido: doc.data().apellido,
            rut: doc.data().rut.toLowerCase(),
            fecha: doc.data().fecha,
            hora: doc.data().hora,
            clasificacion: doc.data().clasificacion
        });
    });
    console.log(listClientes)
    console.log("clientes cargados")
})

db.collection("cupones")
    .onSnapshot(function (queryCup) {
        var listCup = [];
        queryCup.forEach(function (docCup) {
            listCup.push({
                id: docCup.id,
                fecha: docCup.data().fecha,
                hora: docCup.data().hora,
                rut: docCup.data().rut.toLowerCase(),
                nombre: docCup.data().nombre,
                apellido: docCup.data().apellido,
            }
            );
        });

        listCupones = listCup;
    });


function solicitudCupon(rut) {
    var rut = rut.replace(".", "");
    var rut = rut.replace(".", "");
    var rut = rut.replace(".", "");
    validarCliente(rut);
    $('#boton').prop("disabled", true);
}

function eliminarPuntos(dato){
     dato = dato.replace(".", "");
     dato = dato.replace(".", "");
     dato = dato.replace(".", "");
     return dato;
}

function actualizarClientes(list) {
    listClientes = list
}


function iniciarCupones(listCup) {
    listCup = [];
}
function entregarClientes() {
    return listClientes;
}
function entregarCupones() {
    return listCupones;
}

function registrarCliente(rut, nombre, apellido) {
    if (entregarClientes().filter(function (el) {
        return el.rut == rut
    }).length < 1) {
        db.collection("clientes").add({
            rut: rut,
            nombre: nombre,
            apellido: apellido,
            fecha: Date.now()
        })
    } else {
        console.log('uste esta haciendo trampa');
    }
}
functionb validarCliente(rut) {
    listadoFiltroRut = entregarClientes().filter(function (el) {
        return el.rut == rut.toLowerCase();
    });
    if (listadoFiltroRut.length < 1) {
        console.log("Se abre la modal de aviso");
        Swal.fire({
            title: 'Rut no encontrado',
            text: "Desea volver a intentarlo?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si!',

        }).then((result) => {
            if (result.value) {

            } else {
                $('#rut').val('')
            }
            $('#boton').prop("disabled", false);
        })
    } else {

        validarHora(listadoFiltroRut[0]);


    }
}
function modificarCategoria(categoria) {
    switch (categoria) {

        case 'bronce':

            var categoriaActual = db.collection("categorias").doc(localStorage.bronceid);
            monto = parseInt(localStorage.bronceMonto) + parseInt(localStorage.bronce)
            contador = localStorage.contadorBronce + 1;
            return categoriaActual.update({
                monto: monto,
                contador: contador
            })
            break;

        case 'silver':

            var categoriaActual = db.collection("categorias").doc(localStorage.silverid);
            monto = parseInt(localStorage.silverMonto) + parseInt(localStorage.silver)
            contador = localStorage.contadorSilver + 1;
            return categoriaActual.update({
                monto: monto,
                contador: contador
            })
            break;
        case 'golden':

            var categoriaActual = db.collection("categorias").doc(localStorage.goldenid);
            monto = parseInt(localStorage.goldenMonto) + parseInt(localStorage.golden)
            contador = parseInt(localStorage.contadorSilver) + 1;
            return categoriaActual.update({
                monto: monto,
                contador: contador
            })
            break;
        case 'platinium':

            var categoriaActual = db.collection("categorias").doc(localStorage.platinium);
            monto = parseInt(localStorage.platiniumMonto) + parseInt(localStorage.platinium)
            contador = localStorage.contadorPlatinium + 1;
            return categoriaActual.update({
                monto: monto,
                contador: contador
            })
            break;
        default:
            break;
    }
}

function imprimirCupon(cliente) {
    $('#nombreCupon').html(cliente.nombre + " " + cliente.apellido)
    $('#rutCupon').html(cliente.rut.toLowerCase())
    $('#horaCupon').html(obtenerHora());
    $('#fechaCupon').html(obtenerFecha());
    $('#horaSorteo').html(obtenerHora());
    $('#fechaSorteo').html(obtenerFecha());
    clasifi = cliente.clasificacion.toLowerCase();
    switch (clasifi) {
        case 'bronce':
            $('#montoCupon').html(puntos(localStorage.bronce))
            $('#clasificacionCupon').html("Bronce");
            break;
        case 'silver':
            $('#montoCupon').html(puntos(localStorage.silver));
            $('#clasificacionCupon').html("Silver");
            break;

        case 'golden':
            $('#montoCupon').html(puntos(localStorage.golden));
            $('#clasificacionCupon').html("Golden");
            break;

        case 'platinium':
            $('#montoCupon').html(puntos(localStorage.platinium));
            $('#clasificacionCupon').html("Platinium");
            break;
        default:
            break;
    }

    $('#nombreSorteo').html(cliente.nombre + " " + cliente.apellido)
    $('#apellidoSorteo').html(cliente.apellido)
    $('#rutSorteo').html(cliente.rut.toLowerCase())
    $('#divSorteo').removeAttr('hidden');
    $('#divImpresion').removeAttr('hidden');
    $('#divSorteo').printThis();
    setTimeout(function () {

        if (calcularHoraEnMin(obtenerHora()) > 1199) {
            console.log("abriendo sorteo")

            $('#divImpresion').printThis();
        }
    }, 200);

    setTimeout(() => {
        $('#divSorteo').attr('hidden', true);
        $('#divImpresion').attr('hidden', true);
    }, 1000);

}

function generarCupon(cliente) {
    db.collection("cupones").add({
        rut: cliente.rut,
        nombre: cliente.nombre,
        apellido: cliente.apellido,
        fecha: obtenerFecha(),
        hora: obtenerHora()
    }).then(function () {
        $('#rut').val("");
        console.log("generando cupon para:" + cliente.nombre);
        imprimirCupon(cliente);
        var clienteActual = db.collection("clientes").doc(cliente.id);
        return clienteActual.update({
            hora: obtenerHora(),
            fecha: obtenerFecha()
        })
    })


}

function validarMaxCupones(cliente) {
    if (entregarCupones().filter(function (el) {
        return el.fecha == obtenerFecha() && el.rut == cliente.rut.toLowerCase();
    }).length < 2) {
        return true;
    } else {

        return false
    }
}
function validarHora(cliente) {
    nuevoArreglo = entregarClientes().filter(function (el) {
        return el.fecha == obtenerFecha() && el.rut == cliente.rut.toLowerCase();
    })
    if (nuevoArreglo.length < 1) {
        console.log("El cliente : " + cliente.nombre + "No  tiene cupon hoy")
        generarCupon(cliente)
    } else {
        console.log("el cliente si tiene cupon hoy")
        if (calcularHoraEnMin(cliente.hora) < 1199) {
            console.log("el cliente saco el cupon antes de las 20");
            if (calcularHoraEnMin(obtenerHora()) < 1199) {
                $('#boton').prop("disabled", false);
                     $('#rut').val("");
                Swal.fire({
                    title: 'Debe Esperar Hasta las 20:00',
                    text: "Debe Esperar Hasta las 20:00 para sacar otro cupon",
                    type: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                });
            } else {
                diferenciaMin = calcularHoraEnMin(obtenerHora()) - calcularHoraEnMin(cliente.hora);
                if (diferenciaMin > 60) {
                    console.log("generando cupon debido a que a pasado mas de una hora desde que saco cupon ")
                    generarCupon(cliente);
                } else {
                   
                    Swal.fire({
                        title: 'Debe Esperar para volver a sacar otro cupon',
                        text: "Minutos Pendiente: " + diferenciaMin,
                        type: 'warning',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok'
                    });
                    $('#rut').val("");
                    $('#boton').prop("disabled", false);
                }
            }


        } else {
                  $('#rut').val("");
            Swal.fire({
                title: 'Vuelva ma�0�9ana',
                text: "Ya sac�� el maximo de cupones por hoy",
                type: 'warning',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ok'
            });
       
        };

    }

}
