class Categorias {
    constructor() {
        this.listado = [];
    }


    iniciar = () => {
        nucleo.db.collection("categorias").onSnapshot((querys) => {
            this.listado = [];
            querys.forEach((doc) => {
                this.listado.push(doc.data());

                switch (doc.data().nombre) {
                    case 'bronce':
                        $('#montoBronce').html(puntos(doc.data().premio))
                        break;
                    case 'silver':
                        $('#montoSilver').html(puntos(doc.data().premio))
                        break;
                    case 'golden':
                        $('#montoGolden').html(puntos(doc.data().premio))
                        break;
                    case 'platinium':
                        $('#montoPlatinium').html(puntos(doc.data().premio))
                        break;
                    default:
                        break;
                }





            });
            console.log(this.listado)
        });
    }


    filtrar = (dato) => {
        console.log("dato" + dato)
        this.categoriaFiltrada = this.listado.filter((el) => {
            return el.nombre == dato.toLowerCase();
        })
        console.log(this.categoriaFiltrada);

    }
};