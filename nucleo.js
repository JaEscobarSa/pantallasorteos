class Nucleo{
    constructor(){

    }
   
     obtenerHora () {
        var ho = new Date();
        this.hora1 = parseInt(ho.getHours())
        this.minutos1 = parseInt(ho.getMinutes())
        if (this.hora1 < 10) {
            this.hora1 = '0' + this.hora1
        }
        if (this.minutos1 < 10) {
            this.minutos1 = '0' + this.minutos1
        }
        if (this.hora1 == '23' && this.minutos1 == '59') {
            localStorage.controldia = 0;
        }
        this.horaact = this.hora1 + ":" + this.minutos1;
        return this.horaact;
    }

   getHora(hora) {
        return hora.substring(0, 2);
    }
    
     getMinutos(hora) {
        return hora.substring(3, 5);
    
    }
    
}
module.exports = Nucleo;